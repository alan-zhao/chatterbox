#include "chatterbox_mainwindow.h"

extern QList<ChatWindow*> windows;
extern QList<Friends*> friendslst;

Chatterbox_MainWindow::Chatterbox_MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Chatterbox_MainWindow)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(QApplication::applicationDirPath()+"\\EXEICON.ico"));
    this->setCentralWidget(ui->fw);
    this->im=new ItemManager();
    this->nfm=new NewFriendMessage();

    sys=new QSystemTrayIcon(this);
    QIcon icon(QCoreApplication::applicationDirPath()+"\\EXEICON.ico");
    sys->setIcon(icon);
    sys->setToolTip("Chatterbox: "+UserNameLib::getUserName());
    sys->show();

    QObject::connect(this,&Chatterbox_MainWindow::recvMsg,im,&ItemManager::getItem);
    QObject::connect(im,&ItemManager::addFriend,this,&Chatterbox_MainWindow::addFriend);
    QObject::connect(im,&ItemManager::friendRequest,nfm,&NewFriendMessage::addNewFriendRequest);
    QObject::connect(ui->fw,SIGNAL(itemDoubleClicked(QListWidgetItem*)),this,SLOT(doubleclicked(QListWidgetItem*)));
    connect(ui->actionsettings,&QAction::triggered,this,&Chatterbox_MainWindow::settingTriggered);
    connect(ui->actionSearch,&QAction::triggered,this,&Chatterbox_MainWindow::SearchF);
    connect(ui->actionmessage,&QAction::triggered,this,&Chatterbox_MainWindow::messageTriggered);
    connect(ui->actiondeletefriend,&QAction::triggered,this,&Chatterbox_MainWindow::deleteTrigged);
    connect(sys, &QSystemTrayIcon::activated, this, &Chatterbox_MainWindow::activeTray);

    tcpServer=new QTcpServer();
    tcpServer->listen(QHostAddress::AnyIPv4,8888);
    QObject::connect(tcpServer,&QTcpServer::newConnection,this,&Chatterbox_MainWindow::tcpListen);

    udpSocket=new QUdpSocket;
    udpSocket->bind(QHostAddress::AnyIPv4,8888);
    connect(udpSocket,&QUdpSocket::readyRead,this,&Chatterbox_MainWindow::udpListen);
}
void Chatterbox_MainWindow::udpListen(){//Read Udp Datagram
    QByteArray *ba=new QByteArray;
    ba->resize(udpSocket->bytesAvailable()+1);
    QHostAddress address;
    quint16 port;
    udpSocket->readDatagram(ba->data(),udpSocket->bytesAvailable(),&address,&port);
//    while(udpSocket->hasPendingDatagrams())
//    {
//        ba->resize(udpSocket->pendingDatagramSize());

//    }
    emit recvMsg(*ba,tcpSocket);
    qDebug()<<"UDP接收到"<<address<<"::"<<port<<"的连接";
}
void Chatterbox_MainWindow::tcpListen(){//Listen for Tcp
    tcpSocket=tcpServer->nextPendingConnection();
    QString ipAddress=tcpSocket->peerAddress().toString();
    quint16 port=tcpSocket->peerPort();
    qDebug()<<"接收到"+ipAddress+":"+QString::number(port)+"的连接";
    connect(tcpSocket,&QTcpSocket::readyRead,this,&Chatterbox_MainWindow::readText);
}
void Chatterbox_MainWindow::readText(){//Read Tcp Datastream
    static bool opened=0;
    static QFile *file;
    static qint64 fileDownloaded=0;
    static qint64 fileRealSize=0;
    if(opened){
        QByteArray fileContent=tcpSocket->readAll();
        //tcpSocket->flush();
        file->write(fileContent);
        fileDownloaded+=fileContent.size();
        if(fileDownloaded<fileRealSize){
            qDebug()<<"下载文件大小与实际文件大小不符，下载大小为"<<fileContent.size()<<",实际大小为"<<fileRealSize<<"。";
        }else{
            file->close();
            fileRealSize=0;
            fileDownloaded=0;
            opened=0;
            tcpSocket->write("recv");
            tcpSocket->waitForBytesWritten();
        }
        return;
    }
    QByteArray ba;
    ba=tcpSocket->readLine();
    emit this->recvMsg(ba,tcpSocket);
    if(ba.startsWith("file:")){
        auto lst=QString::fromLocal8Bit(ba).split(":");
        auto fileName=lst[3];
        qint64 fileSize=lst[4].toLongLong();
        if(fileSize>100000000){
            return;
        }
        fileRealSize=fileSize;
        QByteArray decodedn=QByteArray::fromBase64(fileName.toLocal8Bit());
        QString xname=QString::fromLocal8Bit(decodedn);
        file=new QFile(QCoreApplication::applicationDirPath()+"\\downloads\\"+xname);
        if(!file->open(QIODevice::WriteOnly|QIODevice::Truncate)){
            qDebug()<<"下载文件失败，无法打开文件";
        }else{
            opened=1;
            QByteArray fileContent=tcpSocket->readAll();
            tcpSocket->flush();
            if(fileContent.size()<fileSize){
                qDebug()<<"下载文件大小与实际文件大小不符，下载大小为"<<fileContent.size()<<",实际大小为"<<fileSize;
                file->write(fileContent);
            }else{
                file->write(fileContent);
                file->close();
                fileDownloaded=0;
                fileRealSize=0;
                opened=0;
                tcpSocket->write("recv");
                tcpSocket->waitForBytesWritten();
            }
            fileDownloaded+=fileContent.size();
        }
    }
}
void Chatterbox_MainWindow::deleteTrigged(){
    if(ui->fw->selectedItems().length()<=0)return;
    auto a=ui->fw->selectedItems()[0];
    for(int i=0;i<this->friends.size();i++){
        if(friends[i].toObject().take("ip").toString("")==qobject_cast<Friends*>(ui->fw->itemWidget(a))->ip){
            friends.removeAt(i);break;
        }
    }
    ui->fw->removeItemWidget(a);
    delete a;
    FileWriter fw(QCoreApplication::applicationDirPath()+"\\data.json");
    QJsonObject json;
    json.insert("name",this->username);
    json.insert("friends",this->friends);
    fw.writeJson(json);
}
void Chatterbox_MainWindow::SearchF(){
    sf=new SearchFriend();
    this->sf->show();
}
void Chatterbox_MainWindow::messageTriggered(){
    this->nfm->show();
}
void Chatterbox_MainWindow::activeTray(QSystemTrayIcon::ActivationReason reason){
    switch (reason)
    {
        case QSystemTrayIcon::DoubleClick:
            show();
            break;
        case QSystemTrayIcon::Trigger:
            show();
            break;
    }
}

void Chatterbox_MainWindow::doubleclicked(QListWidgetItem *item){
    auto k=qobject_cast<Friends*>(ui->fw->itemWidget(item));
    auto window=new ChatWindow(k->ip,
                               k->name);
    k->count=0;
    k->ui->messageCount->setText("");
    k->ui->messageCount->setVisible(0);
    windows.append(window);
    //qDebug()<<windows;
    window->show();
}
void Chatterbox_MainWindow::closeEvent(QCloseEvent *event){
    event->ignore();
    this->hide();
}
Chatterbox_MainWindow::~Chatterbox_MainWindow()
{
    delete ui;
}

void Chatterbox_MainWindow::settingTriggered(){
    qDebug()<<"设置";
    s=new Settings(nullptr);
    s->show();
    QObject::connect(s,&Settings::saveUserNameF,this,&Chatterbox_MainWindow::changeUserName);
}

void Chatterbox_MainWindow::setUp(QString username,QJsonArray friends){
    this->username=username;
    this->friends=friends;
    if(username==""){
        this->username=iplib::getComputerUserName();
    }
    this->setWindowTitle(QString("ChatterBox: ")+this->username);
    for(int i=0;i<this->friends.size();i++){
        QListWidgetItem *item=new QListWidgetItem();
        item->setSizeHint(QSize(0,50));
        ui->fw->addItem(item);
        auto Fw=new Friends(friends.at(i).toObject().value("name").toString(""),friends.at(i).toObject().value("ip").toString(""),ui->fw);
        Fw->ui->messageCount->setVisible(false);
        friendslst.append(Fw);
        ui->fw->setItemWidget(item,Fw);
    }
    ui->fw->setResizeMode(QListView::Adjust);
    ui->fw->setAutoScroll(true);
}
void Chatterbox_MainWindow::changeUserName(QString name){
    this->username=name;
    this->setWindowTitle(QString("ChatterBox: ")+name);
    QJsonObject json;
    UserNameLib::userName=this->username;
    json.insert("name",this->username);
    json.insert("friends",this->friends);
    FileWriter writer(QCoreApplication::applicationDirPath()+"\\data.json");
    writer.writeJson(json);
}
void Chatterbox_MainWindow::addFriend(QString name,QString ip){
    QJsonObject newFr;
    newFr.insert("name",name);
    newFr.insert("ip",ip);
    for(auto i:this->friends){
        if(i.toObject().take("ip").toString("")==ip){
            return;
        }
    }
    this->friends.append(newFr);
    QListWidgetItem *item=new QListWidgetItem();
    item->setSizeHint(QSize(0,50));
    ui->fw->addItem(item);
    auto Fw=new Friends(name,ip,ui->fw);
    Fw->ui->messageCount->setVisible(false);
    ui->fw->setItemWidget(item,Fw);
    friendslst.append(Fw);
    QJsonObject json;
    json.insert("name",this->username);
    json.insert("friends",this->friends);
    FileWriter w(QCoreApplication::applicationDirPath()+"\\data.json");
    w.writeJson(json);
}
