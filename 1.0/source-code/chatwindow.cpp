#include "chatwindow.h"
ChatWindow::ChatWindow(QString ip,QString name) :
    QWidget(nullptr),
    ui(new Ui::ChatWindow)
{
    ui->setupUi(this);
    this->ip=ip;
    setWindowTitle("聊天: "+name);
    this->name=name;
    connect(this->ui->messages,&QTextBrowser::textChanged,this,&ChatWindow::autoStroll);
    FileReader *r=new FileReader(QCoreApplication::applicationDirPath()+"\\history.json");
    auto jobj=r->getJson();
    ui->messages->setHtml(jobj.take(this->ip).toString(""));
}

ChatWindow::~ChatWindow()
{
    delete ui;
}
void ChatWindow::autoStroll(){
    QTextCursor cur=ui->messages->textCursor();
    cur.movePosition(QTextCursor::End);
    ui->messages->setTextCursor(cur);
}

void ChatWindow::on_sendButton_clicked()
{
    QByteArray txt=ui->textInput->toPlainText().toLocal8Bit();
    if(txt.length()>10000)
    {
        QMessageBox::critical(nullptr,"错误","不能发送长度大于10000个字的消息");
        return;
    }
    QString stat;
    QByteArray endoded=txt.toBase64();
    udpSocket=new QUdpSocket();
    udpSocket->connectToHost(this->ip,8888);
    if(udpSocket->waitForConnected())stat="<font color=\"green\"> [发送成功]</font>\n";
    else
    {
        stat="<font color=\"red\"> [发送失败]</font>\n";
        qDebug()<<"发送失败";
        goto closeudp;
    }
    udpSocket->write(("message:"+this->ip+':'+UserNameLib::getUserName()+':'+endoded+':'+QString::number(txt.size())).toLocal8Bit());
    closeudp:
    udpSocket->close();
    ui->textInput->clear();
    ui->messages->setHtml(ui->messages->toHtml()+"<font color=\"blue\">"+UserNameLib::getUserName().toHtmlEscaped()+"</font>\n"+stat+"\n<br />"+QString::fromLocal8Bit(txt).toHtmlEscaped()+"<br />\n<br />");
}
void ChatWindow::closeEvent(QCloseEvent *event)
{
    FileReader *r=new FileReader(QCoreApplication::applicationDirPath()+"\\history.json");
    auto jobj=r->getJson();
    jobj.insert(this->ip,ui->messages->toHtml());
    FileWriter *w=new FileWriter(QCoreApplication::applicationDirPath()+"\\history.json");
    w->writeJson(jobj);
}
void ChatWindow::on_pushButton_clicked()
{
    tcpClient=new QTcpSocket(this);
    tcpClient->connectToHost(ip,8888);
    if(tcpClient->waitForConnected(10000)){}
    else
    {
        qDebug()<<"连接错误";
        return;
    }
    connect(tcpClient,&QTcpSocket::readyRead,this,&ChatWindow::sendFile);
    QStringList a=QFileDialog::getOpenFileNames(nullptr,"选择上传文件",".","*.*");
    this->files=a;
    sendFile();
}
void ChatWindow::sendFile()
{
    if(files.size()==0)
    {
        tcpClient->disconnectFromHost();
        if(tcpClient->state() == QAbstractSocket::UnconnectedState
                || tcpClient->waitForDisconnected(10000)){}
        else{
                qDebug()<<"断开连接错误";
        }
        return;
    }
    QString i=files[0];
    QFile file(i);
    if(!file.open(QIODevice::ReadOnly)){
        qDebug()<<"无法打开文件"<<i;
    }else{
        qDebug()<<"文件名:"<<i;
        if(file.size()>=100000000){
            QMessageBox::critical(nullptr,"错误","不能发送大于100MB的文件");
            file.close();
            return;
        }
        QByteArray ba=file.readAll();
        //qDebug()<<"文件大小:"<<file.size();
        //qDebug()<<"文件内容:"<<ba;
        qDebug()<<"IP地址："<<this->ip;
        auto lst=i.split("/");
        //tcpClient->flush();
        tcpClient->write(("file:"+iplib::GetLocalIpAddress()+":"+UserNameLib::getUserName()+":"+lst[lst.length()-1].toLocal8Bit().toBase64()+":"+QString::number(file.size()).toLocal8Bit()).toLocal8Bit()+"\r\n");
        tcpClient->write(ba);
        QString HtmlText;
        if(tcpClient->waitForBytesWritten(10000))
        {
            qDebug()<<"发送文件成功";
            HtmlText=ui->messages->toHtml()+"<font color=\"blue\">"+UserNameLib::getUserName()+"</font>\n"+"<font color=\"green\"> [发送成功]</font>\n<br />"+"<font color=\"purple\">文件：</font>"+lst[lst.length()-1]+"<br />\n<br />\n";
        }
        else
        {
            qDebug()<<"发送文件失败"<<(tcpClient->state()==QAbstractSocket::UnconnectedState);
            HtmlText=ui->messages->toHtml()+"<font color=\"blue\">"+UserNameLib::getUserName()+"</font>\n"+"<font color=\"red\"> [发送失败]</font>\n<br />"+"<font color=\"purple\">文件：</font>"+lst[lst.length()-1]+"<br />\n<br />\n";
        }
        ui->messages->setHtml(HtmlText);
        file.close();
    }
    files.erase(files.begin());
    if(files.size()==0)
    {
        tcpClient->disconnectFromHost();
        if(tcpClient->state() == QAbstractSocket::UnconnectedState
                || tcpClient->waitForDisconnected(10000)){}
        else{
                qDebug()<<"断开连接错误";
        }
    }
}
