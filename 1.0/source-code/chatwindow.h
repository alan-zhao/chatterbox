#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include <QWidget>
#include <QString>
#include <QList>
#include <QTcpSocket>
#include "ui_chatwindow.h"
#include "connection.h"
#include <QDebug>
#include "usernamelib.h"
#include <QFileDialog>
#include "iplib.h"
#include <QMessageBox>
#include <QUdpSocket>
#include <QCloseEvent>
#include "filewriter.h"
#include "filereader.h"
#include <QJsonObject>
#include <QJsonDocument>

namespace Ui {
class ChatWindow;
}

class ChatWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ChatWindow(QString ip,QString name);
    ~ChatWindow();
    Ui::ChatWindow *ui;
    QString ip;
    QString name;
    void autoStroll();
    void sendFile();
    void closeEvent(QCloseEvent *event);
private slots:
    void on_sendButton_clicked();
    void on_pushButton_clicked();
private:
    QStringList files;
    QTcpSocket *tcpClient;
    QUdpSocket *udpSocket;
};

#endif // CHATWINDOW_H
