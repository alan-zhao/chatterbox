#include "connection.h"
#include <QDebug>
#include <QTcpServer>
#include <QObject>

Connection::Connection(QString ip,QByteArray sends):QObject()
{
    udpClient=new QUdpSocket();
    udpClient->writeDatagram(sends,QHostAddress(ip),8888);
}
Connection::Connection(QString ip,QString sends):QObject()
{
    this->status=Connection(ip,sends.toLocal8Bit()).getLastErr();
}
bool Connection::hasErr(){
    return this->status;
}
int Connection::getLastErr(){
    return this->hasErr();
}
