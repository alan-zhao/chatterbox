#ifndef CONNECTION_H
#define CONNECTION_H

#include <QString>
#include <QTcpSocket>
#include <QTcpServer>
#include <QList>
#include <QObject>
#include <QUdpSocket>

class Connection:public QObject
{
public:
    explicit Connection(QString ip,QByteArray sends);
    explicit Connection(QString ip,QString sends);
    bool hasErr();
    int getLastErr();

private:
    bool status=0;
    QUdpSocket* udpClient;
};

#endif // CONNECTION_H
