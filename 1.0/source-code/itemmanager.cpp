#include "itemmanager.h"
#include <QDebug>
#include "connection.h"
#include <QStringList>
#include "iplib.h"
#include <QFile>
#include <QApplication>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDataStream>
#include "messagedispatcher.h"
#include <QTextCodec>
#include <stdio.h>
ItemManager::ItemManager(QObject *parent) : QObject(parent){}
/*
 * try:127.0.0.1:Alan
 * callback:192.168.1.107:Robert
 * message:192.168.107.1:Lucy:Hello!
 * file:192.168.1.106:Robert:jojo.png:234567\r\n124rde3jixoe9odu39edjxoidjcioe
 *
 *
 */
extern QList<ChatWindow*> windows;
extern QList<Friends*> friendslst;
void ItemManager::getItem(const QByteArray msg,QTcpSocket *tcpSocket){
    qDebug()<<"接收消息:"<<msg;
    if(msg.startsWith("try:")){
        auto lst=QString::fromLocal8Bit(msg).split(":");
        auto ip=lst[1];
        auto userName=lst[2];
        emit friendRequest(userName,ip);
    }else if(msg.startsWith("callback:")){
        auto lst=QString::fromLocal8Bit(msg).split(":");
        auto ip=lst[1];
        auto userName=lst[2];
        emit addFriend(userName,ip);
    }else if(msg.startsWith("message:")){
        auto lst=QString::fromLocal8Bit(msg).split(":");
        auto ip=lst[1];
        auto userName=lst[2];
        QByteArray message=lst[3].toLocal8Bit();
        qint64 messageLen=lst[4].toLongLong();
        QByteArray decoded=QByteArray::fromBase64(message);
        QString xname=QString::fromLocal8Bit(decoded);
        dispatch(ip,userName,xname);
    }else if(msg.startsWith("file:")){
        auto lst=QString::fromLocal8Bit(msg).split(":");
        auto ip=lst[1];
        auto userName=lst[2];
        auto fileName=lst[3];
        QByteArray decodedn=QByteArray::fromBase64(fileName.toLocal8Bit());
        QString xname=QString::fromLocal8Bit(decodedn);
        dispatch(ip,userName,xname,2);
        qDebug()<<decodedn;
    }
}
