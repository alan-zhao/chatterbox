#include "messagedispatcher.h"
#include "usernamelib.h"
#include <chatwindow.h>
#include "ui_chatwindow.h"
#include <QDebug>

QList<ChatWindow*> windows;
QList<Friends*> friendslst;

void dispatch(QString ip,QString sender,QString message,int stat){
    ChatWindow *w;
    extern QList<ChatWindow*> windows;
    extern QList<Friends*> friendslst;
    bool flag=0;
    for(int i=0;i<windows.length();i++){
        if(windows[i]->ip==ip){
            w=windows[i];flag=1;break;
        }
    }
    if(!flag){
        //qDebug()<<QString("分发消息")+"\""+message+"\""+"时出错：无法找到IP地址"+ip;
        auto chat=new ChatWindow(ip,sender);
        chat->show();
        windows.append(chat);
        w=chat;
    }
    QString Html;
    if(stat==1)
    {
        Html=w->ui->messages->toHtml()+"<font color=\"blue\">"+sender.toHtmlEscaped()+"</font>\n<br />"+message.toHtmlEscaped()+"<br />\n<br />";
    }else if(stat==2)
    {
        Html=w->ui->messages->toHtml()+"<font color=\"blue\">"+sender.toHtmlEscaped()+"</font>\n<br />\n<font color=\"purple\">文件：</font>\n"+message.toHtmlEscaped()+"<br />\n<br />";
    }
    w->ui->messages->setHtml(Html);
}
