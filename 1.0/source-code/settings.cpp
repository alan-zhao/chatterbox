#include "settings.h"
#include "ui_settings.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <iplib.h>
#include "filereader.h"
#include <QApplication>
#include <QMessageBox>

Settings::Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    ui->IPAddress->setText(iplib::GetLocalIpAddress());
    FileReader fr(QCoreApplication::applicationDirPath()+"\\data.json");
    auto r=fr.getJson();
    ui->UserName->setText(r.take("name").toString());
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_pushButton_clicked()
{
    //this->parent->setWindowTitle(QString("ChatterBox: ")+ui->UserName->text());
    auto k=ui->UserName->text();
    if(k.contains(':')||k.length()>50)
    {
        QMessageBox::critical(nullptr,"提示","名字不能包含':'且长度不能超过50");
        return;
    }
    this->userName=ui->UserName->text();
    emit saveUserNameF(this->userName);
}
QString Settings::getUserName(){
    return this->userName;
}

